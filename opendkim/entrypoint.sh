#!/usr/bin/env sh

# For a more verbose execution (all commands get printed) in logging (e.g. for debugging) add the set option: set -x (so: set -uxe)
set -ue 

chown opendkim:opendkim /var/db/dkim/private
chmod 700 /var/db/dkim/private
chmod 755 /var/db/dkim/public

# Generate keys
if [ ! -f "/var/db/dkim/private/${ROOT_DOMAIN}.private" ]; then
    opendkim-genkey --restrict --bits=4096 --selector=default --domain=${ROOT_DOMAIN} --directory=/tmp

    mv /tmp/default.private /var/db/dkim/private/${ROOT_DOMAIN}.private
    
    # Extract the public key from the default.txt file and transform into a single line for easy copy/pasting to DNS
    cat /tmp/default.txt | sed -E 's/(^\s+"|"$)//g' | tr -d "\n" > /var/db/dkim/public/${ROOT_DOMAIN}.public
    
    # Make world readable (default rights are 600)
    chmod 644 /var/db/dkim/public/${ROOT_DOMAIN}.public
fi

# start opendkim explicitly because CMD does not have variables knowledge at build time.
opendkim -f -d ${ROOT_DOMAIN} -k /var/db/dkim/private/${ROOT_DOMAIN}.private

