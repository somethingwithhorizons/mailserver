# Something With Horizons (SWH) Mailserver

Welcome!

This (Docker based) mailserver will provide you with an FOSS based e-mail server.

Please be referred to [the Wiki section of this repository](https://gitlab.com/somethingwithhorizons/mailserver/wikis/home) for "extracuricular" information.

## Installation

`git clone git@gitlab.com:somethingwithhorizons/mailserver.git`

## Configuration

### Docker Environment

1. Copy the default template `.env.dist` to a personalisable `.env` file.

2. Fill out all variables in `.env` according to your context.

   #### DNS dependent

   - `ROOT_DOMAIN` The DNS name of your server registered at your DNS registrar (e.g. myregisteredname.com).
   - `SMTP_DOMAIN` The DNS name of your smtp server registered at your DNS registrar (e.g. smtp.myregisteredname.com).
   - `IMAP_DOMAIN` The DNS name of your imap server registered at your DNS registrar (e.g. imap.myregisteredname.com).
   - `WEBMAIL_DOMAIN` The DNS name of your webmail server registered at your DNS registrar (e.g. webmail.myregisteredname.com).

   #### Local server options
   - `SMTP_SSL_CERT_PATH` The SMTP certificate location (in the container).
   - `SMTP_SSL_KEY_PATH` The SMTP key location (in the container).
   - `IMAP_SSL_CERT_PATH` The IMAP certificate location (in the container).
   - `IMAP_SSL_KEY_PATH` The IMAP key location (in the container).
   - `STORAGE_PATH` The location where the account specific data (mails, filters, etc.) is written to (e.g. './vmail').
   - `TIMEZONE` Your server's local [Time zone](https://en.wikipedia.org/wiki/List_of_tz_database_time_zones) (e.g. 'Europe/Amsterdam').
   - `MAIL_UID=5000` [User IDentity](https://en.wikipedia.org/wiki/User_identifier) as which data should be stored.
   - `MAIL_GID=5000` [User Group Identity](https://en.wikipedia.org/wiki/User_identifier) as which data should be stored.

   #### Password generation

   - `ENCRYPTION_SCHEME` The algorithm used for encryption.
   - `ENCRYPTION_ROUNDS` The "depth" of the encryptions defining the both the strength and usage time (e.g. '10').

   #### ISP dependent settings

   - `SMTP_RELAY_HOST` The SMTP smarthost (smtp server) of your ISP (e.g. smtp.yourisp.com).
      > Remember to also include this into your SPF record: 'include:<yourisp.com>' to decrease your outgoing spamscore turning it into: 'v=spf1 mx include:<yourisp.com> -all'!
   - `HEADER_SIZE_LIMIT` The maximal amount of memory in bytes for storing a message header having excess discarded (e.g. '4096000').
   - `RELAY_DESTINATION_CONCURRENCY_LIMIT` The maximum of recipients per message for the relay message delivery transport (e.g. '20').

   - `SMTP_SASL_AUTH_ENABLE` Enable SASL authentication in the Postfix LMTP client (e.g. 'yes' or 'no').
   - `SMTP_SASL_PASSWORD_MAPS` Postfix SMTP [client lookup tables](https://www.postfix.org/postconf.5.html#smtp_sasl_password_maps) (e.g.: 'smtp.yourisp.com username:password').
   - `SMTP_SASL_SECURITY_OPTIONS` Options depend on [smtp_sasl_type](https://www.postfix.org/postconf.5.html#smtp_sasl_type) (e.g. 'noanonymous' for the default cyrus SASL type).


---

### DNS

#### Generic

Set-up the following DNS records at your domain registrar to define the required DNS reference alias mappings, from (sub)domains to specific ip-address or relative to each other:

| Host               |    Type    | Content               |
|:-------------------|:----------:|----------------------:|
| @                  | [MX][1]    | 10 @                  |
| @                  | [A][1]     | `<public IP address>` |
| imap               | [CNAME][1] | @                     |
| smtp               | [CNAME][1] | @                     |
| webmail            | [CNAME][1] | @                     |

> `@` domain root (e.g. example.org) being the alias `MX` source, referring - with priority `10` - to `@` (i.e. the specific 'A' record) as its target. I.e. incomming e-mails go to -> `@` ('A' record; in-turn referring to an IP).
>
> `@` domain root (e.g. example.org) being the alias `A` source, referring to `<public IP address>` (the external ip-address of the mailserver as the target) as its target. I.e. example.org -> 123.234.123.12
>
> `imap`/`smtp`/`webmail` subdomains (i.e. webmail.example.org, etc.) being alias sources, referring to `@` (i.e. the specific 'A' record) as its target. E.g. imap.example.org -> ('A' record; in-turn referring to an IP).

#### DKIM

| Host               |    Type    | Content               |
|:-------------------|:----------:|----------------------:|
| default._domainkey | TXT        |                       |

> `default._domainkey`:
> 
> 1. Start mailserver containers: `docker-compose up`
> 2. Store the contents of the (automatically generated) `config/dkim/public/<ROOT_DOMAIN>.public` in your DNS host's settings value
>    NB:The text between (!) the double quotation marks (so _without_ the quoatation marks themselves)

#### DMARC

| Host               |    Type    | Content               |
|:-------------------|:----------:|----------------------:|
| _dmarc             | TXT        | v=DMARC1; p=none      |

> `v=DMARC1` defines te DMARC version applicable.
>
> `p=none` DMARC doesn’t change how email is handled by the receiver. In other words, no action is taken/messages remain unexamined.
>
> > NB: We used to be much more restrictive:
> >
> > ```code
> > v=DMARC1; p=reject; sp=reject; adkim=s; aspf=s; pct=100; fo=1; rua=mailto:user@example.org; 
> > ```
> >
> > > :warning: Replace `user@example.org` with the account that should receive DMARC reports (2x).
> >
> > `p=reject` if the DKIM check fails, reject the e-mail (for top-level domain).
> >
> > `sp=reject` if the DKIM check fails, reject the e-mail (for sub-level domain).
> >
> > `adkim=s` DKIM domain name must match sender's domain name.
> >
> > `aspf=s` DKIM domain name must match MAIL FROM entry sent by MTA.
> >
> > `pct=100` all mails sent by this domain must pass the DMARC checks.
> >
> > `fo=1` inform the admin if any check fails.
> >
> > `rua=mailto:user@example.org` to what address to send the aggregated reports.
> >
> > `ruf=mailto:user@example.org` to what address to send the failure reports.

#### SPF

The _Sender Policy Framework_ (SPF) is an open standard specifying a technical method to prevent sender address forgery. 
Set-up the following DNS record at your domain registrar:

| Domain  | Type  | Content               |
| --------|:-----:| ---------------------:|
| @       | TXT   | v=spf1 mx -all        |

> :information_source: _The SPF record above tells that we use version `1` of `SPF`, that only the resolved `MX` address is allowed to send e-mails and that everything else should be blocked (`-all`)_.
>
> :information_source: Available options are explained on the [Openspf site](http://www.openspf.org/SPF_Record_Syntax).

### Firewall
Make sure the following `TCP` ports are open/forwarded to your servers:

#### Mailserver
- TCP/25 (SMTP - server to server) <!-- EXPLAIN WHY -->
- TCP/587 (SMTP - client to server) <!-- EXPLAIN WHY -->
- TCP/993 (IMAP - client to server) <!-- EXPLAIN WHY -->
- TCP/4190 (MANAGESIEVE - client to server) <!-- EXPLAIN WHY -->

#### Proxyserver
- TCP/80 (HTTP - client to server) <!-- EXPLAIN WHY (to be able to request Let's Encrypt certificates)? -->
- TCP/443 (HTTPS - client to server) <!-- EXPLAIN WHY (to enable https connections to the webmail)? -->

---

## Operation

### Add account

1. Create password: `make generate-password`

2. Add user to `./config/accounts/accounts.conf` (create file if nonexistant) according to the following syntax:
```
<email-address>|<password>
```

3. Restart mailserver containers: `docker-compose stop && docker-compose up`

### Add alias

1. Add the alias relation to `./config/accounts/aliases.conf` (create file if nonexistant) according to the following syntax:
```
<source@address.tld>|<destination@address.tld>
```

2. Restart mailserver containers: `docker-compose stop && docker-compose up`

### Add transport-map

1. Add the transport-map to `./config/transports/transports.conf` (create file if nonexistant) according to [the following syntax](https://www.postfix.org/postconf.5.html#transport_maps):
```
<to_be_rerouted_domain>|<transport_method>:<transport_server_address>:<port>
```

2. Restart related mailserver containers: `docker-compose stop postfix && docker-compose up postfix`


<!--

## External configurations

## Maintenance

## Troubleshooting

### Logfile analysis

### Situational

-->

<!-- REFERENCES -->
[1]:https://pressable.com/blog/2014/12/23/dns-record-types-explained/
