#!make

include .env

generate-password:
	@read -rp "Enter username (e-mail address): " USERNAME; docker-compose run --rm dovecot doveadm pw -s $(ENCRYPTION_SCHEME) -r $(ENCRYPTION_ROUNDS) -u $${USERNAME}

