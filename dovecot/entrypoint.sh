#!/usr/bin/env sh

# For a more verbose execution (all commands get printed) in logging (e.g. for debugging) add the set option: set -x (so: set -uxe)
set -ue

# Clearing the current userdb content to prevent entry duplication
echo "" > /etc/dovecot/userdb

# Translate the SWH specific use configuration file (single source used by both dovecot and postfix) to Dovecot syntax.

# Split 'login' and 'pass' on the pipe delimiter per line in accounts.conf:
if [ -f /accounts/accounts.conf ]; then
    egrep -v "^\s*$\|^\s*\#" /accounts/accounts.conf | while IFS=$'|' read login pass
    do 
        # Split login into its user and domain parts:
        user=$(echo ${login} | cut -d @ -f1)
        domain=$(echo ${login} | cut -d @ -f2)

        # write the user details into the userdb file
        echo "${login}:${pass}:${MAIL_UID}:${MAIL_GID}::/var/vmail/${domain}/${user}::" >> /etc/dovecot/userdb
    done
else
    echo "WARNING: Missing ./config/accounts/accounts.conf"
fi

# Ensure /var/vmail is owned by the configured UID/GID
if [ "$(stat -c "%u %g" /var/vmail)" != "${MAIL_UID} ${MAIL_GID}" ]; then
  chown -R ${MAIL_UID}:${MAIL_GID} /var/vmail 
fi

# Determine which type of SSL certificates are available and selecting the appropriate one:
# Preference model: Specified > production > test > self-signed
if [[ -n "$IMAP_SSL_CERT_PATH" && -n "$IMAP_SSL_KEY_PATH" ]]; then
  echo "using self-specified certificate location"
  SSL_CERT_PATH=$IMAP_SSL_CERT_PATH
  SSL_KEY_PATH=$IMAP_SSL_KEY_PATH
elif [[ -f "/etc/certs/${IMAP_DOMAIN}/fullchain.pem" && -f "/etc/certs/${IMAP_DOMAIN}/key.pem" ]]; then
  SSL_CERT_PATH="/etc/certs/${IMAP_DOMAIN}/fullchain.pem"
  SSL_KEY_PATH="/etc/certs/${IMAP_DOMAIN}/key.pem"
elif [[ -f "/etc/certs/_test_${IMAP_DOMAIN}/fullchain.pem" && -f "/etc/certs/_test_${IMAP_DOMAIN}/key.pem" ]]; then
  SSL_CERT_PATH="/etc/certs/_test_${IMAP_DOMAIN}/fullchain.pem"
  SSL_KEY_PATH="/etc/certs/_test_${IMAP_DOMAIN}/key.pem"
else
  SSL_CERT_PATH="/etc/certs/_selfsigned_${IMAP_DOMAIN}/fullchain.pem"
  SSL_KEY_PATH="/etc/certs/_selfsigned_${IMAP_DOMAIN}/key.pem"

  if [ ! -d "/etc/certs/_selfsigned_${IMAP_DOMAIN}" ]; then
    mkdir -p "/etc/certs/_selfsigned_${IMAP_DOMAIN}"

    # Create the self-signed key
    openssl req -x509 -newkey rsa:4096 -nodes -days 365 \
      -keyout ${SSL_KEY_PATH} \
      -out    ${SSL_CERT_PATH} \
      -subj   "/CN=${IMAP_DOMAIN}"
  fi
fi

if [[ ! -f "$SSL_CERT_PATH" || ! -f "$SSL_KEY_PATH" ]]; then
  echo "certificates not found..."

  exit 1
fi

# Substitute the applicable paths in the custom configuration file
sed -i s:__SSL_CERT_PATH__:${SSL_CERT_PATH}: /etc/dovecot/conf.d/99-custom.conf
sed -i s:__SSL_KEY_PATH__:${SSL_KEY_PATH}: /etc/dovecot/conf.d/99-custom.conf

exec "$@"

