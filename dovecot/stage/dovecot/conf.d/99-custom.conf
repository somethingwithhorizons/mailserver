# SSL CONFIGURATION 
# =================
# Enforce the use of SSL:
ssl = required

# Designate the ssl certificate that includes the public key to be communicated to clients, and the private key to allow for the actual decryption of client messages
# __VARIABLE__ will be replaced during runtime to refer to the applicable domain
ssl_cert = <__SSL_CERT_PATH__
ssl_key = <__SSL_KEY_PATH__

# Configure some SSL settings to be "more secure": 
ssl_cipher_list = ALL:!LOW:!SSLv2:!EXP:!aNULL
ssl_prefer_server_ciphers = yes

# USER CONFIGURATION
# ==================
# Configures where the user and password databases are found
passdb {
  driver = passwd-file
  args = scheme=SHA512-CRYPT username_format=%u /etc/dovecot/userdb
}

userdb {
  driver = passwd-file
  args = username_format=%u /etc/dovecot/userdb
}

# STORAGE
# =======
# Configure location to store mails (%h is sourced from /etc/dovecot/userdb)
mail_location = maildir:%h/Maildir

# Define mailboxes (directory) client/server mapping and automatic directory subscription
namespace inbox {
  mailbox Drafts {
    auto = subscribe
    special_use = \Drafts
  }
  mailbox Spam {
    special_use = \Junk
  }
  mailbox Junk {
    auto = subscribe
    special_use = \Junk
  }
  mailbox Trash {
    auto = subscribe
    special_use = \Trash
  }
  mailbox Sent {
    auto = subscribe
    special_use = \Sent
  }
  mailbox "Sent Messages" {
    special_use = \Sent
  }
}

# MAIL FILTERING (Sieve)
# ======================

# Enable 'managesieve' protocol:
protocols = $protocols sieve

# Enable 'sieve' plugin:
protocol lmtp {
  mail_plugins = $mail_plugins sieve
}

# Define the path to the directory that will contain the rule set that is to be run after user rules
plugin {
  sieve_after = /etc/dovecot/sieve-after
}

# COMMUNICATION
# =============

# Configure port for postfix to connect for LMTP comms
service lmtp {
  # Create inet listener for TCP/IP communication needed in Docker contexts
  inet_listener lmtp {
    port = 24
  }
}

# Create inet listener for TCP/IP communication needed in Docker contexts
service auth {
  inet_listener {
    port = 12345
  }
}

# LOGGING
# =======
# Write logging to STDOUT and STDERR
info_log_path = /proc/self/fd/1  # I.e. STDOUT
debug_log_path = /proc/self/fd/1 # I.e. STDOUT
log_path = /proc/self/fd/2       # I.e. STDERR
