# This file defines what Sieve should do with mails that are marked as spam
require ["fileinto","mailbox"];

if header :contains ["X-Spam-Status"] "Yes" {
  fileinto :create "Junk";
  stop;
}

