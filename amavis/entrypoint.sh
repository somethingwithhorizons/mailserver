#!/usr/bin/env sh
set -ux

# Update SpamAssassin definitions
sa-update --verbose 

# sa-update returns non-zero exitcodes despite arguably functioning correctly
# (e.g. '1' when an update is not necessary). In this context we're only
# interested in exitcodes higher than '3')
SA_EXIT_CODE=$?
if [ "${SA_EXIT_CODE}" -gt "3" ]; then
  echo "sa-update failed with exit code '${SA_EXIT_CODE}'"
  exit
fi

# Workaround because Amavis can not print to STDOUT.
echo '' > /tmp/amavisd.log
chmod 666 /tmp/amavisd.log
tail -F /tmp/amavisd.log &

exec "${@}"

