#!/usr/bin/env sh

# For a more verbose execution (all commands get printed) in logging (e.g. for debugging) add the set option: set -x (so: set -uxe)
set -ue 

# Clear the user/domain related files
echo "" > /etc/postfix/virtual-alias-maps.cf
echo "" > /etc/postfix/virtual-mailbox-maps.cf
echo "" > /tmp/virtual-mailbox-domains.cf # Used in future transformation (required for the entry deduplication later on)

# Clear the transport maps file
echo "" > /etc/postfix/transport-maps.cf

# per line in the accounts.conf file and split 'login' and 'pass' on the pipe delimiter:
if [ -f "/accounts/accounts.conf" ]; then
    egrep -v "^\s*$\|^\s*\#" /accounts/accounts.conf | while IFS=$'|' read ADDRESS PASSWORD
    do 
        # Split address into its user and domain parts:
        USER=$(echo ${ADDRESS} | cut -d @ -f1)
        DOMAIN=$(echo ${ADDRESS} | cut -d @ -f2)
    
        # Write domains to Postfix' temporary domains file
        echo "${DOMAIN}" >> /tmp/virtual-mailbox-domains.cf

        # Write where to store the e-mail folders (forming the mailbux structure) in Postfix' mailbox mapping file 
        echo "${ADDRESS} ${DOMAIN}/${USER}/" >> /etc/postfix/virtual-mailbox-maps.cf

        # Prevent Postfix' greedy catch-all mechanism (see catch-all footnote [1]):
        echo "${ADDRESS} ${ADDRESS}" >> /etc/postfix/virtual-alias-maps.cf
    done
else
   echo "WARNING: Missing ./config/accounts/accounts.conf"
fi

# Transform the tempory domains file into the definitive one to deduplicate domains
cat /tmp/virtual-mailbox-domains.cf | sort | uniq > /etc/postfix/virtual-mailbox-domains.cf

# Split 'SRC' and 'DEST' on the pipe delimiter per line in aliases.conf:
egrep -v "^\s*$\|^\s*\#" /accounts/aliases.conf | while IFS=$'|' read SRC DEST
do 
    # Write alias to the alias mapping file
    echo "${SRC} ${DEST}" >> /etc/postfix/virtual-alias-maps.cf
done

# Split 'TARGET' and 'TRANSPORT' on the pipe delimiter per line in transports.conf:
egrep -v "^\s*$\|^\s*\#" /transports/transports.conf | while IFS=$'|' read TARGET TRANSPORT
do
    # Write alias to the alias mapping file
    echo "${TARGET} ${TRANSPORT}" >> /etc/postfix/transport-maps.cf
done


# Determine which type of SSL certificates are available and selecting the appropriate one:
# Preferring specified > production > test > self-signed
if [[ -n "$SMTP_SSL_CERT_PATH" && -n "$SMTP_SSL_KEY_PATH" ]]; then
  echo "using self-specified certificate location"
  SSL_CERT_PATH=$SMTP_SSL_CERT_PATH
  SSL_KEY_PATH=$SMTP_SSL_KEY_PATH
elif [[ -f "/etc/certs/${SMTP_DOMAIN}/fullchain.pem" && -f "/etc/certs/${SMTP_DOMAIN}/key.pem" ]]; then
  SSL_CERT_PATH="/etc/certs/${SMTP_DOMAIN}/fullchain.pem"
  SSL_KEY_PATH="/etc/certs/${SMTP_DOMAIN}/key.pem"
elif [[ -f "/etc/certs/_test_${SMTP_DOMAIN}/fullchain.pem" && "/etc/certs/_test_${SMTP_DOMAIN}/key.pem" ]]; then
  SSL_CERT_PATH="/etc/certs/_test_${SMTP_DOMAIN}/fullchain.pem"
  SSL_KEY_PATH="/etc/certs/_test_${SMTP_DOMAIN}/key.pem"
else
  SSL_CERT_PATH="/etc/certs/_selfsigned_${SMTP_DOMAIN}/fullchain.pem"
  SSL_KEY_PATH="/etc/certs/_selfsigned_${SMTP_DOMAIN}/key.pem"
  if [ ! -d "/etc/certs/_selfsigned_${SMTP_DOMAIN}" ]; then
    mkdir -p "/etc/certs/_selfsigned_${SMTP_DOMAIN}"
    # Create the self-signed key
    openssl req -x509 -newkey rsa:4096 -nodes -days 365 \
      -keyout ${SSL_KEY_PATH} \
      -out    ${SSL_CERT_PATH} \
      -subj   "/CN=${SMTP_DOMAIN}"
  fi
fi

if [[ ! -f "$SSL_CERT_PATH" || ! -f "$SSL_KEY_PATH" ]]; then
  echo "certificates not found..."

  exit 1
fi

# Configure where the SSL cert and key can be found
postconf -e "smtpd_tls_cert_file=${SSL_CERT_PATH}"
postconf -e "smtpd_tls_key_file=${SSL_KEY_PATH}"

# Configure relay host (typically used for ISP relay in privately hosted servers)
if [[ -n "${SMTP_RELAY_HOST}" ]]; then
  postconf -e "relayhost=${SMTP_RELAY_HOST}"
fi
if [[ -n "${HEADER_SIZE_LIMIT}" ]]; then
  postconf -e "header_size_limit=${HEADER_SIZE_LIMIT}"
fi
if [[ -n "${RELAY_DESTINATION_CONCURRENCY_LIMIT}" ]]; then
  postconf -e "relay_destination_concurrency_limit=${RELAY_DESTINATION_CONCURRENCY_LIMIT}"
fi

# SASL authentication options (typically used for (free) SMTP relays such as SMTP2GO)
if [[ -n "${SMTP_SASL_AUTH_ENABLE}" ]]; then
  postconf -e "smtp_sasl_auth_enable=${SMTP_SASL_AUTH_ENABLE}"
fi
if [[ -n "${SMTP_SASL_PASSWORD_MAPS}" ]]; then
  postconf -e "smtp_sasl_password_maps=texthash:/etc/postfix/sasl_password_maps.cf"
  echo "${SMTP_SASL_PASSWORD_MAPS}" > /etc/postfix/sasl_password_maps.cf

fi
if [[ -n "${SMTP_SASL_SECURITY_OPTIONS}" ]]; then
  postconf -e "smtp_sasl_security_options=${SMTP_SASL_SECURITY_OPTIONS}"
fi

# Set rDNS of current external IP address hostname to match remote EHLO/HELO checks.
# This ensures that the reverse DNS lookup (i.e. checking for a valid IP address 
# based on hostname) returns a valid result, preventing a mail acceptance score penalty.
# This is done dynamically to support running the mailserver on servers where we cannot
# control the hostname (i.e. most consumer ISP services).
REMOTE_ADDR=$(getent hosts ${ROOT_DOMAIN} | awk {'print $1'})
REMOTE_HOST=$(getent hosts ${REMOTE_ADDR} | awk {'print $2'})
postconf -e "myhostname=${REMOTE_HOST}"

exec "${@}"

# [1]
#
# Postfix grants precedence to alias definitions, preferring them over 
# regular user account definitions when determining e-mail delivery 
# destinations. It does so by checking the alias table before the user 
# table is inquired, instantly acting and delivering the e-mail according 
# to the alias definition should it encounter a match in the alias table 
# (i.e. never turning to the user definitions in that case).
#
# I.O.W: Alias rules overrule user definitions where definitions overlap!
#
# This preference model, by definition, makes a 'catch all alias' (e.g. 
# based on domain: `@example.org john.doe@example.org`) indeed ALWAYS (!) 
# catch ALL e-mail (for that domain in the case of the example) not making 
# exceptions for specific e-mail addresses that MAY BE present in the user 
# table (e.g. e-mail to: `jack.doe@example.org` will be delivered to 
# `john.doe@example.org` even when jack has an account on example.org in 
# the form of: `jack.doe@example.com`)!
#
# This can be countered by simply including all specific user e-mail address
# definitions as aliases to themselves in the alias table:
